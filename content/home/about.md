---
title: "About"
image: "josh-dp.jpg"
weight: 8
---

Hi, I'm Josh; and I'm an information/network security, infrastructure and cloud engineer. I’ve spent the last four years working in cybersecurity startups in Scotland, focusing on data security.

### Background

* BEng (Hons) in Digital Security, Forensics & Ethical Hacking
* Currently doing an MSc in Cybersecurity
* [GIAC Cloud Security Automation certified](https://www.youracclaim.com/badges/e4be92d6-0790-41df-9bbb-86d8403cb1d6/public_url)
* 4+ years working for hi-tech, cybersecurity startups, focusing on application and infrastructure security
* Recovering sysadmin... 🙄
