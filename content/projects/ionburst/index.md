---
title: "Ionburst"
#external_link: "https://ionburst.io"
weight: 1
resources:
    - src: jishfionburstlogo.png
      params:
          weight: 0
---

I am currently the Chief Cybersecurity Engineer for Ionburst, an ultra-secure, private and resilient object store for cloud data.

You can find out more [here.](https://ionburst.io)
