---
title: "dhcp.sucks"
#external_link: "https://dhcp.sucks"
weight: 2
resources:
    - src: comingsoon.jpg
      params:
          weight: -100
---

dhcp.sucks hosts my blog, where I document the projects I'm working on.

You can access it [here.](https://dhcp.sucks)
