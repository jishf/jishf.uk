==================================================================
https://keybase.io/jishf
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://jishf.uk
  * I am jishf (https://keybase.io/jishf) on keybase.
  * I have a public key ASDs79_EwavrCqh-9xguHtR_85mVUIKf3dfEjaPXSysBrwo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0120ecefdfc4c1abeb0aa87ef7182e1ed47ff3999550829fddd7c48da3d74b2b01af0a",
      "host": "keybase.io",
      "kid": "0120ecefdfc4c1abeb0aa87ef7182e1ed47ff3999550829fddd7c48da3d74b2b01af0a",
      "uid": "d937fb84e9f6aefff65bbb59af2d6719",
      "username": "jishf"
    },
    "merkle_root": {
      "ctime": 1591289352,
      "hash": "4aafef1da99b8e043b8e97c6bfb2aedc1440f1053f4b523870523e710f4c22bbf0b343e96e26bd0899f6a21d2ecf779afbb54b24c728b8c62196e9a2f599ff1d",
      "hash_meta": "7df287bb6a3a5bb46865069271b7a92ef24690fe1b64bbba51a1a81de206a8d5",
      "seqno": 16526702
    },
    "service": {
      "entropy": "AZvVrVHEJz8Gbby+hFPH+tSY",
      "hostname": "jishf.uk",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "5.5.0"
  },
  "ctime": 1591289359,
  "expire_in": 504576000,
  "prev": "82d8a95247f8101edc04afa09d091b7663a54afdd439187a4e337023f92c587a",
  "seqno": 7,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEg7O/fxMGr6wqofvcYLh7Uf/OZlVCCn93XxI2j10srAa8Kp3BheWxvYWTESpcCB8QggtipUkf4EB7cBK+gnQkbdmOlSv3UORh6TjNwI/ksWHrEIAkHanGP32Oajs+U/L55ym8F3KAgLG6sZ9uwR6KPnE8iAgHCo3NpZ8RAyGSgYe8JtiSGpUo1oDYedpB+GXOELDNck15FjH3SuoCCHITZsKPkOsRD2JKNbL3drua4IlU+UvbP1au0OVknDahzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIMtNQBdjJXslbpmf9YMehgxIyrUjwbpHbyhGHkC5DM10o3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/jishf

==================================================================